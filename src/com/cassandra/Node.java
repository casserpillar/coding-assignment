package com.cassandra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * The Node class creates objects representing nodes in a graph. A Node contains:
 * <ul>
 *   <li>The name of this node</li>
 *   <li>The index of this node in the graph</li>
 *   <li>A list of the indices of nodes connected to this node</li>
 *   <li>A mapping between the indices of connected nodes and the distance of the route to
 *   that node</li>
 * </ul>
 *
 * @author Cassandra Fantastic
 */
public class Node {
  private String name;
  private Integer index;
  private List<Integer> connectedRoutes;
  private HashMap<Integer, Integer> routeDistances;

  /**
   * Constructs a Node, containing the name of this node and the index of this node in the graph
   * table. The list of connected routes and the mapping between the indices of connected routes and
   * the distances of those routes are initially empty, as nodes are constructed or edited when the
   * graph is constructed or edited by adding/removing routes and distances.
   *
   * @param name    the name of this node
   * @param index   the index of this node in the graph table
   */
  public Node(String name, Integer index) {
    this.name = name;
    this.index = index;
    this.connectedRoutes = new ArrayList<>();
    this.routeDistances = new HashMap<Integer, Integer>();
  }

  /**
   * Gets the name of this node
   * @return the name of this node
   */
  public String getName() {
    return name;
  }

  /**
   * Gets the index of this node in the graph table
   * @return the index of this node in the graph table
   */
  public Integer getIndex() {
    return index;
  }

  /**
   * Gets the list of nodes which are connected to this node
   * @return the list of nodes which are connected to this node
   */
  public List<Integer> getConnectedRoutes() {
    return connectedRoutes;
  }

  /**
   * Gets the hashmap linking the index of a connected node to the distance to that node
   * @return the hashmap linking the index of a connected node to the distance to that node
   */
  public HashMap<Integer, Integer> getRouteDistances() {
    return routeDistances;
  }

  /**
   * Sets a new name for this node
   * @param name the new name of this node
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Sets a new index for this node
   * @param index the new index of this node
   */
  public void setIndex(Integer index) {
    this.index = index;
  }

  /**
   * Sets a new list of nodes connected to this node
   * @param connectedRoutes the new list of nodes connected to this node
   */
  public void setConnectedRoutes(List<Integer> connectedRoutes) {
    this.connectedRoutes = connectedRoutes;
  }

  /**
   * Sets a new hashmap linking the index of a connected node to the distance to that node
   * @param routeDistances the new hashmap linking the index of a connected node to the distance to
   *                       that node
   */
  public void setRouteDistances(HashMap<Integer, Integer> routeDistances) {
    this.routeDistances = routeDistances;
  }

  /**
   * Gets the distance from this node to a connected node
   * @param index the index of the connected node
   * @return the distance from this node to that connected node
   */
  public int getDistance(Integer index) {
    return routeDistances.get(index);
  }

  /**
   * Adds a new route from this node to the given node. This adds the connected node to
   * this.connectedRoutes and adds the distance to that node to the hashmap routeDistances.
   * @param index the index of the new connected node
   * @param distance the distance from this node to that connected node
   */
  public void addRoute(Integer index, Integer distance) {
    if (!connectedRoutes.contains(index)) {
      connectedRoutes.add(index);
    }
    routeDistances.put(index, distance);
  }

  /**
   * Removes the route from this node to the given node. This function removes that node from the
   * list of connected nodes, and deletes the entry of that node from the hashmap giving the
   * distance to connected nodes.
   * @param index the index of the route to be removed
   */
  public void removeRoute(Integer index) {
    connectedRoutes.remove(index);
    routeDistances.remove(index);
  }
}
