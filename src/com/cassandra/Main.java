package com.cassandra;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Main {
  public static void main(String[] args) {
    if (args.length == 0) {
      System.out.println("Please provide a filepath as an argument");
      return;
    }
    String fileString = args[0];
    Path filePath;
    try {
      filePath = Paths.get(fileString);
    } catch(Exception InvalidPathException) {
      System.out.print("Failed: Exception ".concat(InvalidPathException.toString()));
      return;
    }

    List<String> lines;
    try {
      lines = Files.readAllLines(filePath);
    } catch (Exception e) {
      //If the file can't be read from,
      System.out.print("Failed: Exception ".concat(e.toString()));
      return;
    }

//    List<String> routes = new ArrayList<>(
//        Arrays.asList("AB5", "AD2", "AE4", "BC3", "BD6", "BE8", "CE8", "DE6")
//    );
//    RouteResult a, b, c;
//    List<RouteResult> d;
//
//    a = graph.routeDistance(Arrays.asList("C", "B", "A", "E"));
//    System.out.print(routeToString(a).concat("\n"));
//    b = graph.routeDistance(Arrays.asList("D", "E", "A", "B", "D"));
//    System.out.print(routeToString(b).concat("\n"));
//    c = graph.shortestRoute("C", "A");
//    System.out.print(routeToString(c).concat("\n"));
//    d = graph.possibleRoutes("C", "A", 15);
//    for (RouteResult route : d) {
//      System.out.print(routeToString(route).concat("\n"));
//    }

    List<String> graphRoutes = Arrays.asList(lines.get(0).split(" "));
    Graph graph = new Graph(graphRoutes);

    List<String> command;
    String functionToExecute;
    String start;
    String end;
    RouteResult distanceResult;
    RouteResult shortestResult;
    List<RouteResult> possibleRoutes;
    //Iterate through the remaining lines and execute the commands
    for (int i = 1; i < lines.size(); i++) {
      command = Arrays.asList(lines.get(i).split(" "));
      functionToExecute = command.get(0);
      switch (functionToExecute) {
        case "DISTANCE":
          distanceResult = graph.routeDistance(Arrays.asList(command.get(1).split("(?!^)")));
          System.out.println(routeToString(distanceResult));
        case "SHORTEST":
          start = command.get(1).substring(0,1);
          end = command.get(1).substring(1,2);
          shortestResult = graph.shortestRoute(start, end);
          System.out.println(routeToString(shortestResult));
        case "POSSIBLE":
          start = command.get(1).substring(0,1);
          end = command.get(1).substring(1,2);
          Integer maxDistance = Integer.getInteger(command.get(1).substring(2));
          possibleRoutes = graph.possibleRoutes(start, end, maxDistance);
          for (RouteResult route : possibleRoutes) {
            System.out.print(routeToString(route));
            System.out.print("\n");
          }
        default:
          System.out.print("The command failed: ".concat(String.join(" ", command)).concat("\n"));
      }
    }
  }

  static private String routeToString(RouteResult route) {
    String nodeList = String.join("", route.getRoute());
    String distance = route.getDistance().toString();
    return nodeList.concat(distance);
  }
}
