/**
 * Created by Cassandra on 5/09/2016.
 * dunno what licence
 */

package com.cassandra;

import java.util.List;

/**
 * SearchPath objects are used when determining paths between nodes in a graph. A SearchPath object
 * contains the start and end index, the name of all nodes in the path, and the distance covered
 * by the path.
 *
 * @author Cassandra Fantastic
 */

public class SearchPath implements Comparable<SearchPath> {
  private Integer startIndex;
  private Integer endIndex;
  private List<String> route;
  private Integer distance;

  /**
   * Constructs a SearchPath, using the index of the inital node, the index of the final node, a
   * list of the names of all nodes in this path, and the distance covered by this path.
   * @param startIndex  the index of the initial node of this path
   * @param endIndex    the index of the final node of this path
   * @param route       the ordered list of names of the nodes traversed by this path
   * @param distance    the distance covered by this path
   */
  public SearchPath(Integer startIndex, Integer endIndex, List<String> route, Integer distance) {
    this.startIndex = startIndex;
    this.endIndex = endIndex;
    this.route = route;
    this.distance = distance;
  }

  /**
   * Gets the index of the initial node in this path.
   * @return  the index of the initial node of this path
   */
  public Integer getStartIndex() {
    return startIndex;
  }

  /**
   * Gets the index of the final node of this path
   * @return the index of the final node of this path
   */
  public Integer getEndIndex() {
    return endIndex;
  }

  /**
   * Gets the ordered list of names of the nodes traversed by this path
   * @return the ordered list of names of the nodes traversed by this path
   */
  public List<String> getRoute() {
    return route;
  }

  /**
   * Gets the distance covered by this path
   * @return the distance covered by this path
   */
  public Integer getDistance() {
    return distance;
  }

  /**
   * Sets the index of the starting node of this path.
   * @param startIndex the index of the starting node of the path
   */
  public void setStartIndex(Integer startIndex) {
    this.startIndex = startIndex;
  }

  /**
   * Sets the index of the end node of this path
   * @param endIndex the index of the end node of this path
   */
  public void setEndIndex(Integer endIndex) {
    this.endIndex = endIndex;
  }

  /**
   * Sets the ordered list of names of nodes traversed by this path
   * @param route the ordered list of names of nodes traversed by this path
   */
  public void setRoute(List<String> route) {
    this.route = route;
  }

  /**
   * Sets this distance covered by this path
   * @param distance  the distance covered by this path
   */
  public void setDistance(Integer distance) {
    this.distance = distance;
  }

  /**
   * Determines the ordering of two SearchPath objects. Implements Comparable for SearchPath
   * objects. The order is sorted by the distance covered by those routes, from smallest to largest
   * @param path2 the RouteResult that this RouteResult is to be compared to
   */
  @Override
  public int compareTo(SearchPath path2) {
    return this.getDistance().compareTo(path2.getDistance());
  }
}
