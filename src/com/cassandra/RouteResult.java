package com.cassandra;

import java.util.*;

/**
 * RouteResult is the return class for accessor methods in the Graph class. It contains:
 * <ul>
 * <li>An ordered list of node names the route passes through</li>
 * <li>The distance this route covers</li>
 * </ul>
 *
 * @author Cassandra Fantastic
 */
public class RouteResult implements Comparable<RouteResult> {
  private List<String> route;
  private Integer distance;

  /**
   * Constructs a RouteResult, a return object containing a list of node names and a distance.
   *
   * @param route    the ordered list of node names in the route
   * @param distance the distance covered by that route
   */
  public RouteResult(List<String> route, Integer distance) {
    this.route = route;
    this.distance = distance;
  }

  /**
   * Gets the list of node names in the RouteResult
   * @return the list of node names
   */
  public List<String> getRoute() {
    return route;
  }

  /**
   * Gets the distance of the RouteResult
   * @return the distance covered by this route
   */
  public Integer getDistance() {
    return distance;
  }

  /**
   * Sets the list of node names in the RouteResult
   * @param route the list of node names
   */
  public void setRoute(List<String> route) {
    this.route = route;
  }

  /**
   * Sets the distance covered by the RouteResult
   * @param distance the distance covered by this route
   */
  public void setDistance(Integer distance) {
    this.distance = distance;
  }

  /**
   * Determines the ordering of two RouteResult objects. Implements Comparable for RouteResult
   * objects. The order is first sorted by distance, and then alphabetically by node name from first
   * to last node in the paths.
   * @param path2 the RouteResult that this RouteResult is to be compared to
   */
  @Override
  public int compareTo(RouteResult path2) {
    Integer distanceComparison = this.getDistance().compareTo(path2.getDistance());
    Integer result;
    if (distanceComparison != 0) {
      result = distanceComparison;
    } else {
      String v1 = String.join("", this.getRoute());
      String v2 = String.join("", path2.getRoute());
      result = v1.compareTo(v2);
    }
    return result;
  }
}
