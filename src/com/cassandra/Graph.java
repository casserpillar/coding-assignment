package com.cassandra;

import java.util.*;
import java.lang.String;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*
 * Assumptions
 *  Graph won't have to be mutated after creation
 *  The only accessor methods needed are given in the coding assignment
 *      distance, shortest, possible
 *  There is at most one defined route between each pair of nodes in the graph
 *  The routes have single-character names
 *      Expand?
 *
 */

/**
 * This class represents a graph. The constructor will create a undirected weighted graph given a
 * list of nodes and routes, as a List\<String\> containing elements "AB\<number\>", e.g. using
 * {"AC5", "AD14"} will create a graph with nodes A, C, and D and with a 5-distance connection
 * between A-C and a 4-distance connection between A-D.
 *
 * The graph contains:
 * <ul>
 *   <li>A list of the nodes in this graph</li>
 *   <li>A list of the names of the nodes in this graph</li>
 * </ul>
 *
 * Methods exist to search the graph to determine:
 * <ul>
 *   <li>The distance covered by a certain route containing two or more nodes</li>
 *   <li>The shortest route between two given nodes</li>
 *   <li>A list of the routes between two nodes that are less than a maximum distance</li>
 * </ul>
 *
 * @author Cassandra
 */
public class Graph {
  private List<Node> nodes;
  private List<String> names;

  /**
   * Constructs an undirected graph from a list of routes.
   * @param routes A {@code List<String>} of routes with entries of the form "AB15", giving a
   *               single-character names for the start and end nodes of the route, and then a distance
   *               as an integer
   */
  public Graph(List<String> routes) {
    String startNodeName;
    String endNodeName;
    int startNodeIndex;
    int endNodeIndex;
    int distance;

    List<String> newNames = new ArrayList<>();
    List<Node> newNodes = new ArrayList<>();
    /*
     * Parses the list of routes and splits them into two Strings for the start and end node names
     * and an Integer containing the distance of that route, then adds that route to the starting
     * and ending nodes.
     */
    for (String route : routes) {
      startNodeName = route.substring(0, 1);
      endNodeName = route.substring(1, 2);
      distance = Integer.parseInt(route.substring(2));

      if (!newNames.contains(startNodeName)) {
        newNames.add(startNodeName);
        newNodes.add(new Node(startNodeName, newNames.indexOf(startNodeName)));
      }
      if (!newNames.contains(endNodeName)) {
        newNames.add(endNodeName);
        newNodes.add(new Node(endNodeName, newNames.indexOf(endNodeName)));
      }
      startNodeIndex = newNames.indexOf(startNodeName);
      endNodeIndex = newNames.indexOf(endNodeName);
      newNodes.get(startNodeIndex).addRoute(endNodeIndex, distance);
      newNodes.get(endNodeIndex).addRoute(startNodeIndex, distance);
    }
    this.names = newNames;
    this.nodes = newNodes;
  }

  /**
   * Gets the list of nodes in this graph
   * @return the list of nodes in this graph
   */
  public List<Node> getNodes() {
    return nodes;
  }

  /**
   * Gets the list of names of nodes in this graph
   * @return the list of names of nodes in this graph
   */
  public List<String> getNames() {
    return names;
  }

  /**
   * Returns the distance traversed by a route. The route is specified by an ordered list of the
   * names of nodes this route covers.
   * @param routeNodeNames An ordered list of names of nodes this route covers.
   * @return the distance covered by this route
   */
  public RouteResult routeDistance(List<String> routeNodeNames) {
    int distance = 0;
    for (int i = 0; i < routeNodeNames.size() - 1; i++) {
      Integer nodeIndex = names.indexOf(routeNodeNames.get(i));
      Integer nextIndex = names.indexOf(routeNodeNames.get(i + 1));
      distance += nodes.get(nodeIndex).getDistance(nextIndex);
    }
    return new RouteResult(routeNodeNames, distance);
  }

  /**
   * Determines the shortest route between two nodes using the Dijkstra algorithm.
   * @param startNodeName the node at the start of the route
   * @param endNodeName the node at the end of the route
   * @return a RouteResult containing the shortest route between the nodes and the distance covered
   *         by that route
   */
  public RouteResult shortestRoute(String startNodeName, String endNodeName) {
    return dijkstra(startNodeName, endNodeName);
  }

  /**
   * Determines all paths between the two given nodes that have a distance equal to or shorter
   * than the given maximum distance. This method uses a breadth-first search using a priority queue
   * to find all the paths between the given nodes with a distance less than or equal to the maximum
   * distance.
   * @param startNodeName the node to be at the start of each route
   * @param endNodeName the node to be at the end of each route
   * @param maxDistance the maximum distance of each route
   * @return a list of RouteResult objects containing all the routes with a distance equal to or
   * less than the maximum distance, sorted by distance and then alphabetically by node name in the
   * route.
   */
  public List<RouteResult> possibleRoutes(String startNodeName, String endNodeName, int maxDistance) {
    // Breadth-first search without keeping track of visited nodes

    Node currentNode;
    Integer startNodeIndex = names.indexOf(startNodeName);
    Integer endNodeIndex = names.indexOf(endNodeName);

    // Create a priority queue of unchecked paths and poll
    // the first path (the trivial path from the origin to the origin)
    PriorityQueue<SearchPath> paths = new PriorityQueue<>();
    List<String> startPath = new ArrayList<>();
    startPath.add(startNodeName);
    paths.add(new SearchPath(startNodeIndex, startNodeIndex, startPath, 0));
    SearchPath currentPath;
    currentPath = paths.poll();
    currentNode = nodes.get(currentPath.getEndIndex());

    // For this path, consider all of possible extensions and
    // add all new paths to the priority queue. Add these paths
    // to a list of possible paths if they end at the endNode.
    // Then poll the queue for the next-shortest path, and if that
    // path has a distance greater than or equal to the maximum
    // return the list of possible paths.

    List<RouteResult> pathsToDestination = new ArrayList<>();
    Integer newDistance;
    SearchPath newPath;

    Boolean foundAllRoutes = false;
    while (!foundAllRoutes) {
      for (Integer connectedNodeIndex : currentNode.getConnectedRoutes()) {
        newDistance = currentPath.getDistance() + currentNode.getDistance(connectedNodeIndex);
        List<String> newRoute = new ArrayList<String>(currentPath.getRoute());
        newRoute.add(names.get(connectedNodeIndex));
        newPath = new SearchPath(currentPath.getStartIndex(), connectedNodeIndex, newRoute, newDistance);
        paths.add(newPath);

        if (newPath.getEndIndex().equals(endNodeIndex) && newPath.getDistance() <= 15) {
          pathsToDestination.add(new RouteResult(newPath.getRoute(), newPath.getDistance()));
        }
      }

      if (paths.peek().getDistance() >= maxDistance) {
        foundAllRoutes = true;
      } else {
        currentPath = paths.poll();
        currentNode = nodes.get(currentPath.getEndIndex());
      }
    }
    Collections.sort(pathsToDestination);
    return pathsToDestination;
  }

  /**
   * Uses the Dijkstra algorithm to determine the shortest route between two nodes in the
   * graph.
   * @param startNodeName the starting node in the route
   * @param endNodeName the ending node in the route
   * @return the RouteResult containing the shortest route from the start to the end node, and the
   * distance of that route.
   */
  private RouteResult dijkstra(String startNodeName, String endNodeName) {
    Integer startNodeIndex = names.indexOf(startNodeName);
    Integer endNodeIndex = names.indexOf(endNodeName);

    //Assign to each node a distance-guess and a path
    HashMap<Integer, SearchPath> paths = new HashMap<>();
    List<String> currentPath = new ArrayList<>();
    currentPath.add(startNodeName);
    paths.put(startNodeIndex, new SearchPath(startNodeIndex, startNodeIndex, currentPath, 0));

    //Set the initial node as current, and set all other nodes as unvisited
    List<Integer> unvisitedNodes;
    unvisitedNodes = IntStream.range(0, names.size()).boxed().collect(Collectors.toList());
    unvisitedNodes.remove(startNodeIndex);

    //For the current node, consider all of its unvisited neighbors and
    //calculate + update distances + path, and then determine the unvisited
    //node with the shortest path and visit it, until the endNode is visited.
    Integer newDistance;
    Integer currentDistance;
    List<SearchPath> newPathList;
    SearchPath shortestPath;
    SearchPath currentDijkstraPath;

    newPathList = new ArrayList<SearchPath>(paths.values());
    Collections.sort(newPathList);
    Node currentNode = nodes.get(newPathList.get(0).getEndIndex()); //update node from priority queue
    unvisitedNodes.remove(currentNode.getIndex());

    while (unvisitedNodes.contains(endNodeIndex)) {
      for (Integer connectedNodeIndex : currentNode.getConnectedRoutes()) {
        List<String> newPath = new ArrayList<>(currentPath);
        newPath.add(names.get(connectedNodeIndex));
        newDistance = paths.get(currentNode.getIndex()).getDistance()
            + currentNode.getDistance(connectedNodeIndex);
        if (paths.containsKey(connectedNodeIndex)) {
          currentDistance = paths.get(connectedNodeIndex).getDistance();
          if (newDistance < currentDistance) {
            paths.get(connectedNodeIndex).setDistance(newDistance);
            paths.get(connectedNodeIndex).setRoute(newPath);
          }
        } else {
          paths.put(connectedNodeIndex, new SearchPath(startNodeIndex, connectedNodeIndex, newPath, newDistance));
        }
      }

      //sort all paths by length and then visit the closest unvisited node to the origin
      newPathList = new ArrayList<>();
      for (Integer i : unvisitedNodes) {
        if (paths.containsKey(i)) {
          newPathList.add(paths.get(i));
        }
      }
      Collections.sort(newPathList);
      currentDijkstraPath = newPathList.get(0);
      currentPath = currentDijkstraPath.getRoute();
      currentNode = nodes.get(currentDijkstraPath.getEndIndex()); //update node from priority queue
      unvisitedNodes.remove(currentNode.getIndex());
    }
    //When the end node is visited, return that route.
    shortestPath = newPathList.get(0);
    return new RouteResult(shortestPath.getRoute(), shortestPath.getDistance());
  }
}

